#!/bin/bash

aws s3 cp s3://mucherry/terraform/terraform.tfstate .
aws s3 cp s3://mucherry/secrets/mucherry.pem ~/.ssh/id_rsa
sshKeyFile=~/.ssh/id_rsa
wpip=$(jq -r '.resources[] | select (.name == "wordpress") .instances[].attributes.private_ip' terraform.tfstate)
chmod 600 $sshKeyFile
ssh -i $sshKeyFile ec2-user@$wpip $*
rm ~/.ssh/id_rsa /tmp/wpip
