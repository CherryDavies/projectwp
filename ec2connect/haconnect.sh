#!/bin/bash

aws s3 cp s3://mucherry/secrets/mucherry.pem /tmp/mucherry.pem
aws s3 cp s3://mucherry/secrets/haproxyip /tmp/haproxyip
sshKeyFile=/tmp/mucherry.pem
haproxyip=$(cat /tmp/haproxyip)
chmod 600 $sshKeyFile
ssh -i $sshKeyFile ec2-user@$haproxyip
rm /tmp/mucherrykey.pem /tmp/haproxyip
