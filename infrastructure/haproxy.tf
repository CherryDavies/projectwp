resource "aws_instance" "haproxy" {
  ami                    = var.ami
  instance_type          = var.instance_type_a
  key_name               = var.key
  vpc_security_group_ids = [aws_security_group.public_sg.id]
  subnet_id              = aws_subnet.public_subnet_1.id
  iam_instance_profile   = aws_iam_instance_profile.s3_access_profile.name
  user_data              = file("haproxy_userdata.sh")
  tags = {
    Name = "mucherry-haproxy"
  }

}

resource "aws_eip" "haproxy" {
  vpc = true
  tags = {
    Name = "${var.project_name}-haproxy"
  }
}

resource "aws_eip_association" "haproxy" {
  instance_id = aws_instance.haproxy.id
  allocation_id = aws_eip.haproxy.id
}

resource "time_sleep" "haproxy_wait" {
  depends_on      = [aws_eip_association.haproxy]
  create_duration = "1m"
}

resource "local_file" "haproxy_ip_file" {
  depends_on      = [time_sleep.haproxy_wait]
  content         = aws_instance.haproxy.public_ip
  filename        = "/tmp/haproxyip"
  file_permission = "0600"
}

resource "aws_s3_bucket_object" "object" {
  depends_on = [local_file.haproxy_ip_file]
  bucket     = "mucherry"
  key        = "secrets/haproxyip"
  source     = "/tmp/haproxyip"
}
