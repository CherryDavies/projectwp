
#-----------Route53-------------

# App CNAME

resource "aws_route53_record" "elb" {
  zone_id = var.zone_id
  name    = "www.${var.project_name}.${var.domain_name}"
  type    = "A"

  alias {
    name                   = aws_elb.elb.dns_name
    zone_id                = aws_elb.elb.zone_id
    evaluate_target_health = false
  }
}

#RDS CNAME

#resource "aws_route53_record" "rds" {
#  zone_id = var.zone_id
#  name    = "rds.${var.domain_name}.com"
#  type    = "CNAME"
#  ttl     = "300"
#  records = ["${aws_db_instance._______.address}"]
#}
