#--------provider-------
variable "aws_region" {
  type = string
}
variable "ssh_public_key_path" {
  type = string
}
variable "PATH_TO_PUBLIC_KEY" {
  type = string
}
variable "private_key_extension" {
  type = string
}
variable "ssh_key_algorithm" {
  type = string
}
variable "generate_ssh_key" {
  type = bool
}
variable "public_key_extension" {
  type = string
}
variable "bucket_name" {
  type = string
}

variable "aws_profile" {
  type = string
}

#--------VPC--------
data "aws_availability_zones" "available" {}
variable "vpc_cidr" {}
variable "cidrs" {
  type = map
}
variable "office_home" {}

variable "zone_id" {}

#--------DNS-------------
variable "domain_name" {
  type = string
}

#------Instances---------

variable "instance_type_a" {
  type = string
}
variable "instance_type_b" {
  type = string
}
variable "key" {
  type = string
}
variable "ami" {
  type = string
}
variable "ami_bastion" {
  type = string
}

#--------Tags-------------
variable "tags" {
  type = map(string)
  default = {
    Name    = "mucherry"
    Project = "mucherry"
  }
}

#----------project name-------
variable "project_name" {
  type = string
}

#-------S3 Bucket--------
variable "s3_bucket" {
  type = string
}

#---------RDS-------------
variable "snapshotid" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "publicly_accessible" {
  type = bool
}

variable "engine" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "storage_type" {
  type = string
}

variable "allocated_storage" {
  type = string
}

variable "backup_retention_period" {
  type = string
}

variable "backup_window" {
  type = string
}

variable "maintenance_window" {
  type = string
}

variable "maj_eng_ver" {
  type = string
}

variable "parameter_group_name" {
  type = string
}

variable "skip_final_snapshot" {
  type = bool
}

variable "dbusername" {
  type    = string
  default = "admin"
}

variable "dbpassword" {
  type    = string
  default = "mucherry123"
}

##------firewall rules for secgrp------##
variable "port_mysql" {
  type    = string
  default = "3306"
}

variable "port_ssh" {
  type    = string
  default = "22"
}

variable "eip" {
  type    = number
  default = 0
}

#---------ELB-------------
variable "elb_healthy_threshold" {}
variable "elb_unhealthy_threshold" {}
variable "elb_timeout" {}
variable "elb_interval" {}

#---------ASG-------------
variable "asg_max" {}
variable "asg_min" {}
variable "asg_grace" {}
variable "asg_hct" {}
variable "asg_cap" {}
