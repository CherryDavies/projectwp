resource "aws_eip" "natgw" {
  vpc = true
  tags = {
    Name = "${var.project_name}-eip"

  }
}

resource "aws_nat_gateway" "gw" {
  allocation_id = aws_eip.natgw.id
  subnet_id     = aws_subnet.public_subnet_1.id
}
