resource "aws_instance" "wordpress" {
  depends_on             = [local_file.haproxy_ip_file]
  ami                    = var.ami
  instance_type          = var.instance_type_b
  key_name               = var.key
  vpc_security_group_ids = [aws_security_group.private_sg.id]
  subnet_id              = aws_subnet.rds_subnet_1.id
  iam_instance_profile   = aws_iam_instance_profile.s3_access_profile.name
  user_data              = file("wordpress_userdata.sh")
  tags = {
    Name = "mucherry-wordpress"
  }
}
