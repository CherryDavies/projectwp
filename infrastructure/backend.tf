# Add tfstate to bucket
terraform {
  backend "s3" {
    bucket = "mucherry"
    key    = "terraform/terraform.tfstate"
    region = "eu-west-1"
  }
}
