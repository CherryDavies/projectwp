#!/bin/bash

yum -y install jq
yum -y install mysql

yum -y install docker
systemctl enable docker
systemctl start docker
usermod -a -G docker ec2-user
systemctl daemon-reload
systemctl restart docker

aws s3 cp s3://mucherry/terraform/terraform.tfstate .
export theVM_IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4/)
export DOCKER_IP=$(jq -r '.resources[] | select ( .name == "consul" ) .instances[].attributes.private_ip' terraform.tfstate)

docker run -d -v /var/run/docker.sock:/tmp/docker.sock --restart=always --name registrator -h registrator gliderlabs/registrator:latest -ip="$theVM_IP" consul://$DOCKER_IP:8500

cat >/home/ec2-user/createuserwp.sh <<_END_
#!/bin/bash -xv
aws s3 cp s3://mucherry/terraform/terraform.tfstate .
dbpasswordwp=\$(jq -r '.resources[].instances[].attributes | select ( .password != null ) .password' terraform.tfstate)
dbuserwp=\$(jq -r '.resources[].instances[].attributes | select ( .username != null ) .username' terraform.tfstate)
dbserver=\$(jq -r '.resources[].instances[].attributes | select ( .endpoint != null ) .endpoint' terraform.tfstate | sed 's/:.*\$//')
dbname=\$1
dbusername=\$2
dbpassword=\$3
customer=\$4
mysql -h \${dbserver} -u \${dbuserwp} -p\${dbpasswordwp} <<_EOF_
CREATE DATABASE \$dbname;
CREATE USER '\$dbusername'@'%' IDENTIFIED BY '\$dbpassword';
GRANT ALL PRIVILEGES ON \$dbname.* TO '\$dbusername'@'%';
FLUSH PRIVILEGES;
_EOF_
docker run --name \$customer -P -e WORDPRESS_DB_HOST=\${dbserver} -e WORDPRESS_DB_NAME=\$dbname -e WORDPRESS_DB_USER=\$dbusername -e WORDPRESS_DB_PASSWORD=\$dbpassword -e "SERVICE_TAGS=\$customer" -d wordpress:latest
_END_

chmod +x /home/ec2-user/createuserwp.sh
