#Create S3 access role for instances

resource "aws_iam_role" "s3_access_role" {
  name = "S3${var.project_name}Role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
  },
      "Effect": "Allow",
      "Sid": ""
      }
    ]
}
EOF
}

#Create role policy

resource "aws_iam_role_policy" "s3_access_policy" {
  name = "S3${var.project_name}Policy"
  role = aws_iam_role.s3_access_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": "*"
    }
  ]
}
EOF
}

#------------IAM ROLES----------------

#Create S3 access role profile

resource "aws_iam_instance_profile" "s3_access_profile" {
  name = "S3${var.project_name}Profile"
  role = aws_iam_role.s3_access_role.name
}
