resource "aws_security_group_rule" "db-sg-rule-mysql" {
  from_port         = var.port_mysql
  protocol          = "tcp"
  security_group_id = aws_security_group.db-sg.id
  to_port           = var.port_mysql
  type              = "ingress"
  cidr_blocks       = [var.vpc_cidr]
}

resource "aws_security_group_rule" "outbound_rule" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.db-sg.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}
