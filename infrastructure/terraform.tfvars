#-----provider-----
aws_profile = "academy2"
aws_region  = "eu-west-1"
eip         = 0

#-------VPC--------
vpc_cidr = "10.0.0.0/16"
cidrs = {
  public1  = "10.0.1.0/24"
  public2  = "10.0.2.0/24"
  private1 = "10.0.3.0/24"
  private2 = "10.0.4.0/24"
  rds1     = "10.0.5.0/24"
  rds2     = "10.0.6.0/24"
  rds3     = "10.0.7.0/24"
}

bucket_name = "mucherry"

ssh_public_key_path = "/tmp/mucherry"

public_key_extension = "/tmp/mucherry"

ssh_key_algorithm = "RSA"

PATH_TO_PUBLIC_KEY = ""

generate_ssh_key = true

private_key_extension = ".pem"

office_home = ["82.24.141.235/32", "86.4.90.40/32", "86.125.62.70/32", "82.24.122.149/32", "86.157.127.85/32"]

zone_id = "Z07626429N74Z31VDFLI"

#--------DNS-------------
domain_name = "academy.grads.al-labs.co.uk"

#--------Instance Default-------------
instance_type_a = "t2.micro"
instance_type_b = "t3a.medium"
ami             = "ami-0ea3405d2d2522162"
key             = "mucherry"

#-------Instance Bastion--------
ami_bastion = "ami-0ea3405d2d2522162"

#-----------Project name-----------
project_name = "mucherry"
#-------S3 Bucket--------
s3_bucket = "mucherry"

#---------ELB-------------
elb_healthy_threshold   = "2"
elb_unhealthy_threshold = "2"
elb_timeout             = "3"
elb_interval            = "30"

#---------ASG-------------
asg_max   = "2"
asg_min   = "1"
asg_grace = "300"
asg_hct   = "EC2"
asg_cap   = "2"

#----------DB---------------
snapshotid              = null
instance_class          = "db.t2.micro"
publicly_accessible     = true
engine                  = "mariadb"
engine_version          = "10.2.21"
storage_type            = "gp2"
allocated_storage       = 5
backup_retention_period = 30
backup_window           = "23:30-00:30"
maintenance_window      = "Sat:02:00-Sat:05:00"
maj_eng_ver             = 10.2
parameter_group_name    = "default.mariadb10.2"
skip_final_snapshot     = false
