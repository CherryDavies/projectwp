resource "aws_instance" "consul" {
  ami                    = var.ami
  instance_type          = var.instance_type_a
  key_name               = var.key
  vpc_security_group_ids = [aws_security_group.private_sg.id]
  subnet_id              = aws_subnet.rds_subnet_1.id
  user_data              = file("consul_userdata.sh")
  tags = {
    Name = "mucherry-consul"
  }
}
