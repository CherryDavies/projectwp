#!/bin/bash -xv

aws s3 cp s3://mucherry/secrets/mucherry.pem ~/.ssh/id_rsa

yum -y install mysql
yum -y install jq

yum -y install haproxy
systemctl enable haproxy

wget https://releases.hashicorp.com/consul-template/0.25.0/consul-template_0.25.0_linux_amd64.tgz -O /tmp/consul-template.tgz
tar xvf /tmp/consul-template.tgz
mv consul-template /bin

cat >/home/ec2-user/wpconnect.sh <<_EOF_
#!/bin/bash
aws s3 cp s3://mucherry/terraform/terraform.tfstate .
aws s3 cp s3://mucherry/secrets/mucherry.pem ~/.ssh/id_rsa
sshKeyFile=~/.ssh/id_rsa
wpip=\$(jq -r '.resources[] | select (.name == "wordpress") .instances[].attributes.private_ip' terraform.tfstate)
chmod 600 \$sshKeyFile
ssh -i \$sshKeyFile ec2-user@\$wpip \$*
rm ~/.ssh/id_rsa /tmp/wpip
_EOF_

chmod +x /home/ec2-user/wpconnect.sh

cat >/bin/haproxytmplt <<_EOF_
#!/bin/bash
(
aws s3 cp s3://mucherry/terraform/terraform.tfstate /tmp/terraform.tfstate
/bin/consul-template -consul-addr=\$(jq -r '.resources[] | select ( .name == "consul" ) .instances[].attributes.private_ip' /tmp/terraform.tfstate):8500 -template="/etc/haproxy/haproxy.ctmpl:/etc/haproxy/haproxy.cfg:systemctl reload haproxy"
)&
_EOF_

chmod +x /bin/haproxytmplt

cat >/etc/systemd/system/consul-template.service <<_EOF_
[Unit]
Description=HAProxy Consul Template
After=haproxy.service


[Service]
Type=simple
RemainAfterExit=yes
ExecStart=/bin/haproxytmplt


[Install]
WantedBy=multi-user.target
_EOF_

systemctl enable consul-template.service

cat >/etc/haproxy/haproxy.ctmpl <<_EOF_
#---------------------------------------------------------------------
# Global settings
#---------------------------------------------------------------------
global
    log         127.0.0.1 local2
    chroot      /var/lib/haproxy
    pidfile     /var/run/haproxy.pid
    maxconn     4000
    user        haproxy
    group       haproxy
    daemon
    # turn on stats unix socket
    stats socket /var/lib/haproxy/stats


#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------
defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 3000

#---------------------------------------------------------------------
# main frontend which proxys to the backends
#---------------------------------------------------------------------
frontend  main *:80
    {{ range service "wordpress" }}
    use_backend bk_http_{{ .ID | regexReplaceAll ".*:(.*):.*" "\$1"}} if { hdr(host) -i {{ .Tags | join "" }} }
    {{ end }}
    default_backend  static

#---------------------------------------------------------------------
# static backend for serving up images, stylesheets and such
#---------------------------------------------------------------------
backend static
    balance     roundrobin
    server      static 127.0.0.1:4331 check

# Backend configuration
{{ range service "wordpress" }}
backend bk_http_{{ .ID | regexReplaceAll ".*:(.*):.*" "\$1"}}
  balance roundrobin
  server {{ .ID | regexReplaceAll ".*:(.*):.*" "\$1"}} {{ .Address }}:{{ .Port }} check
{{ end }}
_EOF_

systemctl start consul-template.service
systemctl start haproxy
