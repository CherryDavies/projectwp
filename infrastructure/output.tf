output "haproxyip" {
  value = aws_instance.haproxy.public_ip
}

output "wpip" {
  value = aws_instance.wordpress.private_ip
}
