#-------------Create VPC-------------

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "${var.project_name}-VPC"
  }
}

#-------------Create internet gateway-------------

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.project_name}-InternetGateway"
  }
}

#-------------Create Route tables-------------

#Public Route Table
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  tags = {
    Name = "${var.project_name}-PublicRouteTable"
  }
}

# Private Route Table
resource "aws_default_route_table" "private_rt" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id

  tags = {
    Name = "${var.project_name}-PrivateRouteTable"
  }
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gw.id
  }

}

#-------------Create Subnets---------------

#Create Public Subnet 1

resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["public1"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "${var.project_name}-PublicSubnet1"
  }
}

#RDS Private Subnet 1

resource "aws_subnet" "rds_subnet_1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["rds1"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "${var.project_name}-RDSSubnet1"
  }
}

#RDS Private Subnet 2

resource "aws_subnet" "rds_subnet_2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["rds2"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "${var.project_name}-RDSSubnet2"
  }
}

#------------Create Subnet Associations---------------

# Public subnet 1 Association with Public Route Table
resource "aws_route_table_association" "public_assoc_1" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_rt.id
}


#------------Create RDS Subnet group---------------
#Defining which subnets should be used for RDS autoscaling_groups

resource "aws_db_subnet_group" "rds_subnetgroup" {
  name = "${var.project_name}-rds_subnetgrp"

  subnet_ids = ["${aws_subnet.rds_subnet_1.id}",
    "${aws_subnet.rds_subnet_2.id}"
  ]

  tags = {
    Name = "${var.project_name}-RDSSubnetGroup"
  }
}


#---------Add key pair key to bucket------
#resource "aws_s3_bucket_object" "key" {
#  bucket = var.s3_bucket
#  key    = "${var.key}.pem"
#  source = "${data.external.pwd.result.dir}/${var.key}"

#  etag = filemd5("${data.external.pwd.result.dir}/${var.key}")
#}
