resource "aws_security_group" "db-sg" {
  name   = "${var.project_name}-DBSecurityGroup"
  vpc_id = aws_vpc.vpc.id
}
