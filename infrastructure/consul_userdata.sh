#!/bin/bash

yum -y install docker
systemctl enable docker
systemctl start docker
usermod -a -G docker ec2-user
systemctl daemon-reload
systemctl restart docker


export DOCKER_IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4/)

docker run -p 8400:8400 -p 8500:8500 -p 8600:53/udp -h consul --restart=always --name consul progrium/consul -server -advertise $DOCKER_IP -bootstrap 
