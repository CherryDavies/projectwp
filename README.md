# MUCHERRYS VENTURES

## Description
This repository automates the deployment of an RDS database server, a Consul server, HAProxy and WordPress, each in the form of their own instance on AWS.

Multiple WordPress Docker containers are launched on the WordPress instance, whereby each container is connected to its own database within the RDS database server (which uses MariaDB as its engine).
Every time a WordPress Docker container is launched or started/restarted, HAProxy and MariaDB are automatically configured.

A combination of AWS, Docker, Terraform, Consul and Mysql is used to enable this.

## Prerequisites

* An S3 bucket on AWS
* An ssh key stored in the S3 bucket on AWS

## Documentation

1. Change the variables in the terraform.tfvars file to correspond to yours
2. Initialise terraform by running the following command:
```
terraform init
```
3. Create all of the infrastructure by running the following command:
```
terraform apply
```
This launches everything outlined in the description above (apart from the individual WordPress Docker containers)

### Launching WordPress Docker containers

1. ssh in to HAProxy by running the following commands:
```
aws s3 cp s3://mucherry/secrets/<keyname> /tmp/<keyname>
```
```
sshKeyFile=/tmp/<keyname>
```
```
chmod 600 $sshKeyFile
```
```
ssh -i $sshKeyFile ec2-user@<keyname>
```
```
rm /tmp/<keyname>
```
where <keyname> is the name of your key

2. ssh on to Wordpress by running the wpconnect.sh script:
```
./wpconnect
```

3. run the following command:
```
./createuserwp.sh <databasename> <username> <password> <DNSname>
```
* where <databasename> is the name you wish to call your database
* where <username> is the name you wish to call your username
* where <password> is the name you wish to call your password
* where <DNSname> is the name you wish to call your website

You will need to manually add the DNS name via Route 53 on AWS and your Wordpress website should now be online
